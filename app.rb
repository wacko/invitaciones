require "cuba"
require "mote"
require "cuba/contrib"
require "rack/protection"
require "shield"
require "ohm"
require "ohm/contrib"
require "scrivener"
require "mailgun"
require "csv"
require "dotenv"

Encoding.default_external = "UTF-8"
Dotenv.load

ROOT = File.expand_path("../", __FILE__)
HOST_URL = ENV.fetch("HOST_URL")
REDIS_URL = ENV.fetch("REDIS_URL")
APP_SECRET = ENV.fetch("APP_SECRET")
DOWNLOAD_FILE = ENV.fetch("DOWNLOAD_FILE")
MAIL_DOMAIN = ENV.fetch("MAIL_DOMAIN")
MAILGUN_API_KEY = ENV.fetch("MAILGUN_API_KEY")

Cuba.plugin Cuba::Mote
Cuba.plugin Cuba::TextHelpers
Cuba.plugin Shield::Helpers

Ohm.redis = Redic.new(REDIS_URL)

# Require all application files.
Dir["./models/**/*.rb"].each  { |rb| require rb }
Dir["./routes/**/*.rb"].each  { |rb| require rb }
Dir["./services/**/*.rb"].each  { |rb| require rb }

# Require all helper files.
Dir["./helpers/**/*.rb"].each { |rb| require rb }
Dir["./filters/**/*.rb"].each { |rb| require rb }

Cuba.plugin HtmlHelpers

Cuba.use Rack::MethodOverride
Cuba.use Rack::Session::Cookie,
  key: "invitations",
  secret: APP_SECRET

Cuba.use Rack::Protection
Cuba.use Rack::Protection::RemoteReferrer

Cuba.use Rack::Static,
  urls: %w[/js /css /images],
  root: "./public"

Cuba.define do
  persist_session!

  on root do
    render "signup", invitation: InvitationForm.new({})
  end
  on get, "signup" do
    render "signup", invitation: InvitationForm.new({})
  end

  on post, "signup" do
    form = InvitationForm.new(req.params['invitation'])
    if form.valid?
      invitation = Invitation.create(form.attributes)
      session[:message] = "Invitacion enviada"
      MailSender.new.send_invitation invitation
      res.redirect "/"
    else
      render "signup", invitation: form
    end
  end

  on "download/:track_id" do |track_id|
    invitation = Invitation.find(hash_id: track_id).first
    if invitation
      invitation.downloaded = true
      invitation.save
      res.redirect DOWNLOAD_FILE
    else
      not_found!
    end
  end

  on "webhook" do
    token, timestamp, signature = req.params.values_at('token', 'timestamp', 'signature')
    if WebHook.verify token, timestamp, signature
      recipient, event = req.params.values_at('recipient', 'event')
      WebHook.record recipient, event
      res.status = 200
    end
  end

  on "tracking" do
    render "tracking", invitations: Invitation.all
  end

  on "tracking.csv" do
    res.headers["Content-Type"] ||= "text/csv"
    csv  = CSV.generate do |csv|
      csv << ['nombre', 'email', 'apm', 'state', 'download']
      Invitation.all.each do |row|
        csv << row.to_csv
      end
    end
    res.write csv
  end
end
