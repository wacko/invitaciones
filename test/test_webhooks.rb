require "cutest"
require_relative "../models/invitation"
require_relative "../services/web_hook"

MAILGUN_API_KEY="key-3ax6xnjp29jd6fds4gc373sgvjxteol0"

test "signature" do
  timestamp = "1431374019"
  token = "34410c5114e1d5f98ebe5e7907d3dfc676813a99633aaf13d1"
  signature = "d962c47c5b25ea96f453268cefdc03ab22afadb80bca726e25b9a78e57a380fa"

  assert WebHook.verify(token, timestamp, signature)
end

test "record" do
  begin
    email = "webhook@example.com"
    invitation = Invitation.create email: email

    WebHook.record email, 'bounce'

    assert_equal 'failed', invitation.load!.state
  ensure
    invitation.delete    
  end
end
