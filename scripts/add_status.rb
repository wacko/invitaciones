require_relative "../app"
require "csv"

def parse_csv file
  puts("Processing #{file} ..")

  csv_file = File.join(__dir__, 'data', file)
  CSV.foreach(csv_file, headers: :first_row) do |row|
    email = row[0]
    puts "--> #{email}"
    invitation = Invitation.find(email: email).first
    yield invitation
  end  
end

# members_sent.csv => 926 usuarios a los ya se les mandó el mail
# members_opened.csv => 346 usuarios que abrieron el mail
# members_clicked.csv => 150 usuarios que clickearon el mail

parse_csv('members_sent.csv') {|inv| inv.update_state 'accepted'}
parse_csv('members_opened.csv') {|inv| inv.update_state 'opened'}
parse_csv('members_clicked.csv') do |inv|
  inv.downloaded = true
  inv.state = 'clicked'
  inv.save
end

# Sent mail to all the new users
Invitation.all.each do |invitation|
  if invitation.state.nil?
    MailSender.new.send_invitation invitation
  end
end
