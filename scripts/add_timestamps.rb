require_relative "../app"

now = Time.now.utc.to_i
Invitation.all.each do |record|
  record.created_at = now
  record.save
end
