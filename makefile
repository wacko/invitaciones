gems:
	which gs  || gem install gs
	which dep || gem install dep
	dep add cuba
	dep add mote
	dep add cuba-contrib
	dep add rack-protection
	dep add shield
	dep add ohm
	gs init

install:
	dep install

console:
	irb -r ./app

restart:
	touch tmp/restart.txt
