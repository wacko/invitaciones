class InvitationForm < Scrivener
  attr_accessor :name, :surname, :apm, :email

  def validate
    assert_present :name
    assert_present :apm
    assert_present :email
    assert_email :email
  end
end
