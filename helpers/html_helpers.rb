module HtmlHelpers
  HTML_ESCAPE = {
    "&" => "&amp;",
    ">" => "&gt;",
    "<" => "&lt;",
    '"' => "&quot;",
    "'" => "&#x27;",
    "/" => "&#x2F;"
  }

  UNSAFE = /[&"'><\/]/

  def h(s)
    return '' unless s
    s.to_str.gsub(UNSAFE, HTML_ESCAPE)
  end

  def not_found!
    res.status = 404
    res.headers["Content-Type"] ||= "text/html; charset=utf-8"
    res.write partial("404")
    halt res.finish
  end

end
