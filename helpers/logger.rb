require 'logger'

module MyLogger
  def self.setup(app)
    rack_env = ENV['RACK_ENV'] || 'development'
    app.settings[:logger] ||= Logger.new("log/#{rack_env}.log")
  end

  def logger
    settings[:logger]
  end
end
