module Mailer
  extend Mote::Helpers

  def self.render(template, params = {})
    mote("#{ROOT}/mails/%s.mote" % template, params)
  end
end
