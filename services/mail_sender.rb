class MailSender

  def initialize
    @client = Mailgun::Client.new MAILGUN_API_KEY
  end

  def send_invitation invitation
    text = Mailer.render("invitation", { invitation: invitation })
    puts "Sending mail to #{invitation.email}"

    message_params = {from:    'glaucopoenapps@axistotal.com.ar',
                      to:      invitation.email,
                      subject: 'Descargue GlaucoPoen Apps desde su celular!',
                      text:    text,
                      html:    text}

    @client.send_message MAIL_DOMAIN, message_params
  rescue e
    "Error sending invitation to #{invitation.email}"
    puts e
  end

end
