require 'openssl'

module WebHook

  EVENT_TO_STATE = {
    'bounce' => 'failed',
    'deliver' => 'accepted',
    'drop' => 'failed',
    'spam' => 'complained',
    'unsubscribe' => 'unsubscribed',
    'click' => 'clicked',
    'open' => 'opened'
  }

  def self.verify token, timestamp, signature
    digest = OpenSSL::Digest::SHA256.new
    data = [timestamp, token].join
    signature == OpenSSL::HMAC.hexdigest(digest, MAILGUN_API_KEY, data)
  end

  def self.record recipient, event
    invitation = Invitation.find(email: recipient).first
    invitation.update_state EVENT_TO_STATE[event]
    invitation
  end

end
