require 'ohm'
require 'ohm/contrib'

class Invitation < Ohm::Model
  include Ohm::Callbacks
  include Ohm::Timestamps

  attribute :name
  attribute :surname
  attribute :apm
  attribute :email
  attribute :downloaded
  attribute :hash_id
  attribute :state # => new bounce deliver drop spam unsubscribe click open

  index :email
  index :hash_id

  def app_link
    "https://#{HOST_URL}/download/#{hash_id}"
  end

  def update_state state
    set :state, state
  end

  def to_csv
    [name, email, apm, state, (downloaded ? "si" : "no")]
  end

protected

  def before_create
    self.state = "new"
  end

  def after_create
    self.hash_id = Digest::MD5.hexdigest id
    save
  end

end
